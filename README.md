# MoodTracker

Simple Mood Tracker application 

## To serve locally

Execute `npm install`
Serve with `ng serve`

## Architecture

Backend was developed with firebase functions. The application have a simple authentication process. Once you register you can start to use the app and add entries to the list of moods.



