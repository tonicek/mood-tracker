import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

const API_KEY = '337A254ED086';

const apiKeyIsCorrect = (key: string) => {
  return key === API_KEY;
};

// Initialize Firebase instance
admin.initializeApp();

// Initialize the database and authentication
const database = admin.firestore();
const auth = admin.auth();

// Creating express instance
const app = express();
app.use(cors());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
  );

  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Creating the main endpoint
export const api = functions.https.onRequest(app);

// Get user account
app.get('/user/:uid', async (req, res) => {
  try {
    const user = await database
      .collection('users')
      .doc(req.params.uid)
      .get();
    return res.json(user.data());
  } catch (error) {
    return res.json({
      error: 'Error getting user',
    });
  }
});

// Register a new user account
app.post('/register', async (req, res) => {
  const { email, password, displayName, apiKey } = req.body;

  if (!apiKeyIsCorrect(apiKey)) {
    return res.json({
      error:
        'The provided key is missing or incorrect. Contact administrators.',
    });
  }

  try {
    const user = await auth.createUser({
      disabled: false,
      password: password,
      displayName: displayName,
      email: email,
      emailVerified: true,
    });
    return res.send(user);
  } catch (error) {
    return res.json(error);
  }
});

// Get user mood list
app.get('/mood/list/', async (req, res) => {
  const { userId, apiKey } = req.query;

  if (!apiKeyIsCorrect(apiKey)) {
    return res.json({
      error:
        'The provided key is missing or incorrect. Contact administrators.',
    });
  }

  const collection = await database
    .collection('users')
    .doc(userId)
    .collection('moods')
    .orderBy('created', 'desc')
    .get();
  const moods = collection.docs.map(doc => doc.data());
  return res.json(moods);
});

// Add new mood
app.post('/mood/add', async (req, res) => {
  const { userId, value, feelings, comment, apiKey } = req.body;
  if (!apiKeyIsCorrect(apiKey)) {
    return res.json({
      error:
        'The provided key is missing or incorrect. Contact administrators.',
    });
  }

  const doc = await database
    .collection('users')
    .doc(userId)
    .collection('moods')
    .add({
      created: Date.now(),
      value,
      feelings,
      comment,
    });

  return res.json({
    id: doc.id,
  });
});

// Actividadores

// On user create
exports.createUserRegister = functions.auth.user().onCreate(async user => {
  const { displayName, uid } = user;
  await database
    .collection('users')
    .doc(uid)
    .set({
      uid,
      displayName,
    });
});
