import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { moduleRef } from 'src/testing.module';
import { Store } from '@ngxs/store';
import { UserState } from './states/user.state';
import { SetUser } from './actions/user.actions';
import { User } from './models/user.model';
import { getDebugNode, DebugElement } from '@angular/core';

export const USER_STATE = {
  user: null,
};

describe('AppComponent', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule(moduleRef).compileComponents();
    store = TestBed.get(Store);
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'mood-tracker'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('mood-tracker');
  }));
  fit('should render LoginViewComponent if there is not user', async(() => {
    const fixture = TestBed.createComponent(AppComponent);

    store.dispatch(new SetUser(null));
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    store
      .selectOnce(state => state.user.user)
      .subscribe(value => {
        expect(value).toBe(null);
        const el: HTMLElement = compiled.querySelector('app-login-view');
        expect(el.tagName.toLowerCase()).toBe('app-login-view');
      });
  }));

  fit('should render HomeViewComponent if there is user', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const user = {
      uid: 'tony',
      displayName: 'Anthony Diaz',
      moods: [],
      averageMood: 0,
      totalMoods: 0,
      token: 'jwt',
    };

    store.dispatch(new SetUser(user));
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    store
      .selectOnce(state => state.user.user)
      .subscribe(value => {
        const el: HTMLElement = compiled.querySelector('app-home-view');
        expect(el.tagName.toLowerCase()).toBe('app-home-view');
        expect(value).toEqual(user);
        expect(el).not.toBe(null);
        expect(value.uid).toEqual('tony');
      });
  }));
});
