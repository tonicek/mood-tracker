import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

const apiKey = '337A254ED086';

export class APIInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const payload = req.body;

    let body = { ...payload, apiKey };
    const user = JSON.parse(localStorage['user'] || '{}');

    if (
      req.method === 'POST' ||
      req.method === 'PUT' ||
      req.method === 'DELETE'
    ) {
      if (user.uid) {
        body = {
          ...body,
          userId: user.uid,
          token: user.token,
        };
      }
    }

    const request = req.clone({
      url: `https://us-central1-mood-tracker-tony.cloudfunctions.net/api${
        req.url
      }${
        req.method === 'GET' ? '?userId=' + user.uid + '&apiKey=' + apiKey : ''
      }`,
      headers: headers,
      body: body,
    });

    return next.handle(request);
  }
}
