import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as firebase from 'firebase';
import 'firebase/auth';
import { LoginPayload } from '../models/login.model';
import { RegisterPayload } from '../models/register.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  login(payload: LoginPayload) {
    const { email, password } = payload;
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  getUser(uid: string) {
    return this.http.get(`/user/${uid}`).toPromise();
  }

  register(payload: RegisterPayload) {
    return this.http.post('/register', payload);
  }

  logout() {
    return firebase.auth().signOut();
  }
}
