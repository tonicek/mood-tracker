import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mood } from '../models/mood.model';
import { CreateMoodResponse } from '../models/create-mood-response.modelt';
import { ApiResponse } from '../models/api-response.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getMoods() {
    return this.http.get<Mood[]>('/mood/list');
  }

  createMood(mood: Mood) {
    return this.http.post<CreateMoodResponse>('/mood/add', mood);
  }
}
