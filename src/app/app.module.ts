import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatToolbarModule,
  MatGridListModule,
  MatChipsModule,
  MatTabsModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatProgressBarModule,
  MatIconModule,
  MatMenuModule,
} from '@angular/material';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { LayoutModule } from '@angular/cdk/layout';
import { AreaChartModule, NgxChartsModule } from '@swimlane/ngx-charts';
import * as firebase from 'firebase';

import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MoodSliderComponent } from './components/mood-slider/mood-slider.component';
import { MoodOptionComponent } from './components/mood-option/mood-option.component';
import { MoodItemComponent } from './components/mood-item/mood-item.component';
import { HomeViewComponent } from './views/home-view/home-view.component';
import { environment } from 'src/environments/environment';
import { InfoToolbarComponent } from './components/info-toolbar/info-toolbar.component';
import { DialogTemplateComponent } from './views/dialog-template/dialog-template.component';
import { AuthenticationState } from './states/authentication.state';
import { UserState } from './states/user.state';
import { APIInterceptor } from './interceptors/url.interceptor';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { EmojifyPipe } from './pipes/emojify.pipe';

firebase.initializeApp({
  apiKey: 'AIzaSyCXeUIA8Z-YVmtLsd12Y06WEOq6F_WPxD8',
  authDomain: 'mood-tracker-tony.firebaseapp.com',
  databaseURL: 'https://mood-tracker-tony.firebaseio.com',
  projectId: 'mood-tracker-tony',
  storageBucket: 'mood-tracker-tony.appspot.com',
  messagingSenderId: '867489661393',
});

@NgModule({
  declarations: [
    AppComponent,
    MoodSliderComponent,
    MoodOptionComponent,
    MoodItemComponent,
    HomeViewComponent,
    InfoToolbarComponent,
    DialogTemplateComponent,
    LoginViewComponent,
    EmojifyPipe,
  ],
  entryComponents: [DialogTemplateComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatGridListModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatChipsModule,
    LayoutModule,
    NgxChartsModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTabsModule,
    NgxsModule.forRoot([AuthenticationState, UserState], {
      developmentMode: !environment.production,
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({}),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: APIInterceptor, multi: true },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000 } },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
