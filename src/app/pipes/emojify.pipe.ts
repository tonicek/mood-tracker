import { Pipe, PipeTransform } from '@angular/core';
import { Mood } from '../enum/mood.enum';

@Pipe({
  name: 'emojify',
})
export class EmojifyPipe implements PipeTransform {
  transform(value: number): string {
    let emoji: string;
    switch (Math.round(value)) {
      case 0:
      case Mood.VeryBad:
        emoji = '😔';
        break;
      case Mood.Bad:
        emoji = '😥';
        break;
      case Mood.Dizzy:
        emoji = '😐';
        break;
      case Mood.Normal:
        emoji = '🙂';
        break;
      case Mood.Good:
        emoji = '😀';
        break;
      case Mood.VeryGood:
        emoji = '😉';
        break;

      default:
        emoji = '😎';
        break;
    }

    return emoji;
  }
}
