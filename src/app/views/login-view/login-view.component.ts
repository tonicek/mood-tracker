import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as firebase from 'firebase';
import { Register, Login } from 'src/app/actions/authentication.actions';
import { RegisterPayload } from 'src/app/models/register.model';
import { AuthenticationState } from 'src/app/states/authentication.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss'],
})
export class LoginViewComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;
  @Select(AuthenticationState.loading) loading$: Observable<boolean>;

  constructor(private store: Store, private fb: FormBuilder) {
    this.initializeForms();
  }

  login() {
    const { email, password } = this.loginForm.value;
    this.store.dispatch(new Login({ email, password }));
  }

  register() {
    const { email, displayName, password } = this.registerForm.value;
    this.store.dispatch(new Register({ email, displayName, password }));
  }

  ngOnInit() {}

  private initializeForms() {
    this.loginForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.email, Validators.required]),
      ],
      password: [null, Validators.required],
    });

    this.registerForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.email, Validators.required]),
      ],
      password: [null, Validators.required],
      displayName: [null, Validators.required],
    });
  }
}
