import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/states/user.state';
import { Observable } from 'rxjs';
import { Mood } from 'src/app/models/mood.model';
import { GetMoods } from 'src/app/actions/user.actions';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss'],
})
export class HomeViewComponent implements OnInit {
  @Select(UserState.moods) moods$: Observable<Mood[]>;
  @Select(UserState.loading) loading$: Observable<boolean>;

  constructor(public dialog: MatDialog, private store: Store) {
    this.store.dispatch([new GetMoods()]);
  }

  showNote(mood: Mood) {
    alert(mood.comment);
  }

  ngOnInit() {}
}
