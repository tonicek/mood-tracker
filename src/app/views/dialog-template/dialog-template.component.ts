import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { BreakpointObserver } from '@angular/cdk/layout';
import {
  Component,
  Inject,
  OnInit,
  Renderer2,
  ViewChildren,
  QueryList,
  ElementRef
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { CreateMood } from 'src/app/actions/user.actions';
import { Mood } from 'src/app/models/mood.model';
import { DialogData } from '../home-view/home-view.component';

@Component({
  selector: 'app-dialog-template',
  templateUrl: './dialog-template.component.html',
  styleUrls: ['./dialog-template.component.scss'],
  animations: [
    trigger('status', [
      state(
        'initial',
        style({
          transform: 'skew(0)',
          fontStyle: 'normal',
          padding: 0
        })
      ),
      state(
        'selected',
        style({
          transform: 'skew(-15deg)',
          fontStyle: 'italic',
          padding: '5px 10px',
          backgroundColor: '#fa8100',
          color: 'white'
        })
      ),
      transition('hold => free', [animate('150ms')]),
      transition('free => hold', [animate('150ms')])
    ])
  ]
})
export class DialogTemplateComponent implements OnInit {
  @ViewChildren('feelRef') feelingsRefs: QueryList<ElementRef>;
  feelings = [
    'Happy',
    'Sad',
    'Angry',
    'Calm',
    'Serious',
    'Shocked',
    'Proud',
    'Thankful',
    'Interested',
    'Playful'
  ];
  feelSelected = [];
  comment = new FormControl('');
  moodValue = 0;
  sliderWidth = 300;
  cols = 5;

  constructor(
    public dialogRef: MatDialogRef<DialogTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private store: Store,
    private actions$: Actions,
    private breakpointObserver: BreakpointObserver,
    private renderer: Renderer2
  ) {
    this.actions$
      .pipe(ofActionSuccessful(CreateMood))
      .subscribe(({ payload }) => this.dialogRef.close());

    if (this.breakpointObserver.isMatched('(max-width: 599px)')) {
      this.sliderWidth = 225;
      this.cols = 2;
    }
  }

  isSelected(feel: string) {
    return this.feelSelected.some(feeling => feeling === feel);
  }

  toggleSelection(feel: string, idx: number) {
    const isSelected = this.isSelected(feel);
    const { nativeElement } = this.feelingsRefs.find(
      (_, index) => index === idx
    );

    if (isSelected) {
      this.feelSelected = this.feelSelected.filter(f => f !== feel);
      this.renderer.removeClass(nativeElement, 'selected');
    } else {
      this.renderer.addClass(nativeElement, 'selected');
      this.feelSelected.push(feel);
    }
  }

  setMoodValue(val: number) {
    this.moodValue = val || 0;
  }

  addMood() {
    const feelings =
      this.feelSelected.length > 3
        ? this.feelSelected.slice(0, 3)
        : this.feelSelected;

    const mood: Mood = {
      comment: this.comment.value,
      created: firebase.firestore.Timestamp.now().toDate(),
      feelings: feelings,
      value: this.moodValue || 0.1
    };

    this.store.dispatch(new CreateMood(mood));
  }

  ngOnInit() {}
}
