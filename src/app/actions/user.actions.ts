import { User } from '../models/user.model';
import { Mood } from '../models/mood.model';

export class SetUser {
  static readonly type = '[User] SetUser';
  constructor(public readonly user: User) {}
}
export class GetMoods {
  static readonly type = '[User] GetMoods';
}

export class CreateMood {
  static readonly type = '[User] CreateMood';
  constructor(public mood: Mood) {}
}
