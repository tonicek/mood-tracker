import { LoginPayload } from '../models/login.model';
import { RegisterPayload } from '../models/register.model';

export class Login {
  static readonly type = '[Authentication] Login';
  constructor(public readonly payload: LoginPayload) {}
}

export class Register {
  static readonly type = '[Authentication] Register';
  constructor(public readonly payload: RegisterPayload) {}
}

export class Logout {
  static readonly type = '[Authentication] Logout';
}
