import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { UserState } from './states/user.state';
import { Observable } from 'rxjs';
import { User } from './models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @Select(UserState.user) user$: Observable<User>;
  title = 'mood-tracker';
}
