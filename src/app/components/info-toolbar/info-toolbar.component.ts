import { Component, OnInit, HostListener } from '@angular/core';
import * as shape from 'd3-shape';
import { MatDialog } from '@angular/material/dialog';
import { DialogTemplateComponent } from 'src/app/views/dialog-template/dialog-template.component';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'src/app/states/user.state';
import { Observable } from 'rxjs';
import { ChartData } from 'src/app/models/chart-data.model';
import { Logout } from 'src/app/actions/authentication.actions';
import { User } from 'src/app/models/user.model';
import { Mood } from 'src/app/models/mood.model';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-info-toolbar',
  templateUrl: './info-toolbar.component.html',
  styleUrls: ['./info-toolbar.component.scss'],
})
export class InfoToolbarComponent implements OnInit {
  @Select(UserState.chartData) data$: Observable<ChartData[]>;
  @Select(UserState.moodsAverage) average$: Observable<number>;
  @Select(UserState.moods) moods$: Observable<Mood[]>;
  @Select(UserState.user) user$: Observable<User>;
  showChart = true;
  scheme = {
    name: 'neons',
    selectable: false,
    group: 'Ordinal',
    domain: [
      '#3f51b5',
      '#FF33FF',
      '#CC33FF',
      '#0000FF',
      '#33CCFF',
      '#33FFFF',
      '#33FF66',
      '#CCFF33',
      '#FFCC00',
      '#FF6600',
    ],
  };
  curve: any = shape.curveBasis;

  @HostListener('window:resize')
  onresize(evt: Event) {
    this.showChart = window.innerWidth > 700;
  }

  constructor(public dialog: MatDialog, private store: Store) {
    this.showChart = window.innerWidth > 700;
  }

  openDialog(): void {
    this.dialog.open(DialogTemplateComponent, {
      width: '800px',
      closeOnNavigation: false,
      disableClose: true,
      position: {
        top: '40px',
      },
    });
  }

  logout() {
    this.store.dispatch(new Logout());
  }

  ngOnInit() {}
}
