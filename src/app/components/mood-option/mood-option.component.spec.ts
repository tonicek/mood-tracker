import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodOptionComponent } from './mood-option.component';

describe('MoodOptionComponent', () => {
  let component: MoodOptionComponent;
  let fixture: ComponentFixture<MoodOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
