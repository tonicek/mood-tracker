import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mood-option',
  templateUrl: './mood-option.component.html',
  styleUrls: ['./mood-option.component.scss']
})
export class MoodOptionComponent implements OnInit {
  @Input() text: string;
  constructor() { }

  ngOnInit() {
  }

}
