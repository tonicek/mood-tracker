import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
  Renderer2,
  AfterViewInit,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-mood-slider',
  templateUrl: './mood-slider.component.html',
  styleUrls: ['./mood-slider.component.scss'],
  animations: [
    trigger('moveUp', [
      state(
        'hold',
        style({
          transform: 'translateY(-25px)',
          cursor: 'grabbing'
        })
      ),
      state(
        'free',
        style({
          transform: 'translateY(0)',
          cursor: 'grab'
        })
      ),
      transition('hold => free', [animate('150ms')]),
      transition('free => hold', [animate('150ms')])
    ])
  ]
})
export class MoodSliderComponent implements OnInit, AfterViewInit {
  @Input() width = 300;
  @Input() value: number;
  @Output() selected = new EventEmitter<number>();
  @ViewChild('wrapper') wrapper: ElementRef;
  @ViewChild('indicator') indicator: ElementRef;
  currentValue = '😔';
  currentColor = '#F2FDE4';
  state = 'free';
  mouseDown = false;
  lastPosition = 0;
  progress = 0;

  private _states: Array<string> = ['😔', '😥', '😐', '🙂', '😀', '😉', '😎'];
  private _colors: Array<string> = [
    '#F2FDE4',
    '#DEFABB',
    '#C6F68D',
    '#AAF255',
    '#75E900',
    '#61D800',
    '#41C300'
  ];
  private _parentX = -25;
  private _parentWidth = 0;
  private _maxValue = 0;

  @HostListener('window:mouseup')
  private onMouseup() {
    this.emitValue();
  }

  constructor(private renderer: Renderer2) {
    this._parentWidth = this.width ? this.width : 300;
  }

  setMousedown() {
    this.mouseDown = true;
    this.state = 'hold';
  }

  onPan(evt: any) {
    // Hammer event
    this.moveSlider(evt.center.x);
  }

  emitValue() {
    this.mouseDown = false;
    this.state = 'free';
    this.selected.emit(this.value);
  }

  ngOnInit() {}

  ngAfterViewInit() {
    const { x, width } = this.wrapper.nativeElement.getBoundingClientRect();
    this._parentX = x;
    this._parentWidth = width;
    this._maxValue = width - x + 30;
  }

  private moveSlider(x: number) {
    const statesLength = this._states.length;
    const { left, right } = this.wrapper.nativeElement.getBoundingClientRect();
    let indicatorMiddlePoint = x - this._parentWidth;

    if (x < left) {
      indicatorMiddlePoint = -20;
    } else if (x > left && x < right) {
      indicatorMiddlePoint = x - left - 25;
    } else {
      indicatorMiddlePoint = this._parentWidth - 30;
    }

    const value =
      (indicatorMiddlePoint * statesLength - 1) / (this._parentWidth - 30);
    this.value = value < 0 ? 0 : Math.round(value * 100) / 100;
    const currentIndex =
      Math.round(this.value) > this._states.length - 1
        ? this._states.length - 1
        : Math.round(this.value);
    this.currentValue = this._states[currentIndex];
    this.currentColor = this._colors[currentIndex];
    this.progress = indicatorMiddlePoint + 30;

    this.renderer.setStyle(
      this.indicator.nativeElement,
      'left',
      `${indicatorMiddlePoint}px`
    );
  }
}
