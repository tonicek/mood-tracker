import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodSliderComponent } from './mood-slider.component';

describe('MoodSliderComponent', () => {
  let component: MoodSliderComponent;
  let fixture: ComponentFixture<MoodSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
