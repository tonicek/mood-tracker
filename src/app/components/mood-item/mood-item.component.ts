import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mood-item',
  templateUrl: './mood-item.component.html',
  styleUrls: ['./mood-item.component.scss']
})
export class MoodItemComponent implements OnInit {
  @Input() text: string;
  constructor() { }

  ngOnInit() {
  }

}
