import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodItemComponent } from './mood-item.component';

describe('MoodItemComponent', () => {
  let component: MoodItemComponent;
  let fixture: ComponentFixture<MoodItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
