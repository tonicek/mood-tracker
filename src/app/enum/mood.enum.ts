export enum Mood {
    VeryBad = 1,
    Bad,
    Dizzy,
    Normal,
    Good,
    VeryGood,
    Excellent
}
