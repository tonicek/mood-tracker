import { State, Selector, Action, StateContext } from '@ngxs/store';
import { UserService } from '../services/user.service';
import { SetUser, GetMoods, CreateMood } from '../actions/user.actions';
import { Mood } from '../models/mood.model';
import { User } from '../models/user.model';
import { EmojifyPipe } from '../pipes/emojify.pipe';

export interface UserStateModel {
  user: User;
  moods: Mood[];
  requesting: boolean;
  loading: boolean;
}

@State<UserStateModel>({
  name: 'user',
  defaults: {
    user: localStorage['user'] && JSON.parse(localStorage['user']),
    moods: JSON.parse(localStorage['moods'] || '[]'),
    requesting: false,
    loading: false,
  },
})
export class UserState {
  constructor(private userService: UserService) {}

  @Selector()
  static user(state: UserStateModel) {
    return state.user;
  }

  @Selector()
  static moods({ moods }: UserStateModel) {
    return moods;
  }

  @Selector()
  static loading({ loading }: UserStateModel) {
    return loading;
  }

  @Selector()
  static moodsAverage({ moods }: UserStateModel) {
    const average =
      moods.map(mood => mood.value).reduce((a, b) => a + b) / moods.length;
    return Math.round(average * 100) / 100;
  }

  @Selector()
  static chartData({ moods }: UserStateModel) {
    return [
      {
        name: 'Mood',
        series: moods
          .slice(0, 10)
          .reverse()
          .map(mood => {
            const date = new Date(mood.created);
            return {
              name: this.formatDate(date),
              value: Math.round(mood.value),
            };
          }),
      },
    ];
  }

  static formatDate(date: Date) {
    return `${date.getHours() < 10 ? '0' : ''}${date.getHours()}:${
      date.getMinutes() < 10 ? '0' : ''
    }${date.getMinutes()}`;
  }

  @Action(SetUser)
  setUser({ patchState }: StateContext<UserStateModel>, { user }: SetUser) {
    localStorage['user'] = JSON.stringify(user);
    patchState({
      user,
    });
  }

  @Action(GetMoods)
  getMoods({ patchState }: StateContext<UserStateModel>) {
    patchState({
      loading: true,
    });
    return this.userService.getMoods().subscribe((moods: Mood[]) => {
      localStorage['moods'] = JSON.stringify(moods);
      patchState({
        moods,
        loading: false,
      });
    });
  }

  @Action(CreateMood)
  createMood(
    { patchState, getState }: StateContext<UserStateModel>,
    { mood }: CreateMood
  ) {
    patchState({
      requesting: true,
    });

    return this.userService.createMood(mood).subscribe(response => {
      const state = getState();
      const id = response.id;
      patchState({
        moods: [{ ...mood, id }, ...state.moods],
        requesting: false,
      });
    });
  }
}
