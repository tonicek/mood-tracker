import { State, Selector, Action, StateContext } from '@ngxs/store';
import { AuthenticationService } from '../services/authentication.service';
import { Login, Register, Logout } from '../actions/authentication.actions';
import { SetUser } from '../actions/user.actions';
import { MatSnackBar } from '@angular/material';
import { NgZone } from '@angular/core';
import { FirebaseError } from 'firebase';
import { User } from '../models/user.model';

export interface AuthenticationStateModel {
  loading: boolean;
}

@State<AuthenticationStateModel>({
  name: 'authentication',
  defaults: {
    loading: false,
  },
})
export class AuthenticationState {
  constructor(
    private authService: AuthenticationService,
    private snackBar: MatSnackBar,
    private zone: NgZone
  ) {}

  @Selector()
  static loading({ loading }: AuthenticationStateModel) {
    return loading;
  }

  @Action(Login)
  async login(
    { patchState, dispatch }: StateContext<AuthenticationStateModel>,
    { payload }: Login
  ) {
    patchState({
      loading: true,
    });

    try {
      const { user } = await this.authService.login(payload);
      const session = await this.authService.getUser(user.uid);
      dispatch(new SetUser(session as User));
    } catch (error) {
      this.zone.run(() => {
        this.snackBar.open((<FirebaseError>error).message);
      });
    }

    patchState({
      loading: false,
    });
  }

  @Action(Register)
  register(
    { patchState, dispatch }: StateContext<AuthenticationStateModel>,
    { payload }: Register
  ) {
    patchState({
      loading: true,
    });

    this.authService.register(payload).subscribe(response => {
      if (response['error']) {
        return this.snackBar.open(response['error']);
      }
      dispatch(new SetUser(response as User));
    });
  }

  @Action(Logout)
  logout({ patchState, dispatch }: StateContext<AuthenticationStateModel>) {
    localStorage.clear();
    location.reload();
  }
}
