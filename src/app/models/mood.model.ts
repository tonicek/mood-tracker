export interface Mood {
  id?: string;
  comment: string;
  feelings: Array<string>;
  value: number;
  created: Date;
}
