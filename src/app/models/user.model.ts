import { Mood } from './mood.model';

export interface User {
    uid: string;
    displayName: string;
    moods: Array<Mood>;
    averageMood: number;
    totalMoods: number;
    token: string;
}
