export interface RegisterPayload {
    displayName: string;
    email: string;
    password: string;
}
