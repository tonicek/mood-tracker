export interface ChartData {
  name: string;
  series: ChartDataSerie;
}

export interface ChartDataSerie {
  name: string;
  value: number;
}
