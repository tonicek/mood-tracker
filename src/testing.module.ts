import { AppComponent } from './app/app.component';

import { MoodSliderComponent } from './app/components/mood-slider/mood-slider.component';

import { MoodOptionComponent } from './app/components/mood-option/mood-option.component';

import { MoodItemComponent } from './app/components/mood-item/mood-item.component';

import { HomeViewComponent } from './app/views/home-view/home-view.component';

import { InfoToolbarComponent } from './app/components/info-toolbar/info-toolbar.component';

import { DialogTemplateComponent } from './app/views/dialog-template/dialog-template.component';

import { LoginViewComponent } from './app/views/login-view/login-view.component';

import { EmojifyPipe } from './app/pipes/emojify.pipe';

import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatDialogModule,
  MatGridListModule,
  MatToolbarModule,
  MatProgressBarModule,
  MatChipsModule,
  MatIconModule,
  MatMenuModule,
  MatSnackBarModule,
  MatTabsModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material';

import { LayoutModule } from '@angular/cdk/layout';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { NgxsModule } from '@ngxs/store';

import { AuthenticationState } from './app/states/authentication.state';

import { UserState } from './app/states/user.state';

import { environment } from './environments/environment';

import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { APIInterceptor } from './app/interceptors/url.interceptor';

import { APP_BASE_HREF } from '@angular/common';

export const moduleRef = {
  declarations: [
    AppComponent,
    MoodSliderComponent,
    MoodOptionComponent,
    MoodItemComponent,
    HomeViewComponent,
    InfoToolbarComponent,
    DialogTemplateComponent,
    LoginViewComponent,
    EmojifyPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatGridListModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatChipsModule,
    LayoutModule,
    NgxChartsModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTabsModule,
    NgxsModule.forRoot([AuthenticationState, UserState], {
      developmentMode: !environment.production,
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({}),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true,
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: { duration: 3000 },
    },
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
};
